import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { MatSliderChange } from '@angular/material/slider';
import { HeaderComponent } from 'layout';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    @ViewChild(MatSidenavContainer, { static: true }) sidenav: MatSidenavContainer;
    @ViewChild(HeaderComponent, { static: true }) header: HeaderComponent;

    size = 2;
    logged = false;

    extendedHeader = true;

    messages = 1;

    menuLeft = [
        {
            icon: 'mail',
            route: 'opt1',
            parameters: {},
            label: 'Option 1',
            active: false,
            elementos: 0
        },
        {
            icon: 'home',
            route: 'opt2',
            parameters: {},
            label: 'Option 2',
            active: false,
            elementos: 2,
            children: [
                {
                    icon: 'person',
                    route: 'opt2.1',
                    parameters: {},
                    label: 'Option 2.1',
                    active: false,
                    elementos: 0
                },
                {
                    icon: 'home',
                    route: 'opt2.2',
                    parameters: {},
                    label: 'Option 2.2',
                    active: false,
                    elementos: 0
                }
            ]
        },
        {
            icon: 'person',
            route: 'opt3',
            parameters: {},
            label: 'Option 3',
            active: false,
            elementos: 0
        },
        {
            icon: 'star',
            route: 'opt4',
            parameters: {},
            label: 'Option 4',
            active: false,
            elementos: 0
        },
        {
            icon: 'face',
            route: 'opt5',
            parameters: {},
            label: 'Option 5',
            active: false,
            elementos: 0
        }
    ];

    profile = {
        funcionario: {
            nombre: 'Juan Pablo León Bazante',
            dependencia: 'Control Interno',
            cargo: 'Jefe de Control Int'
        },
        secciones: [
            {
                tipo: 'always',
                elementos: [
                    {
                        active: false,
                        icon: 'mail',
                        label: 'Mensajes',
                        route: '/mensajes',
                        parameters: {},
                        elementos: 3
                    },
                    {
                        icon: 'account_box',
                        label: 'Profile',
                        route: '/profile',
                        parameters: {},
                        elementos: 0
                    }
                ]
            },
            {
                tipo: 'extended',
                elementos: []// this.mainMenu
            }
        ]
    };

    cliente = {
        nombre: 'Kishron SAS',
        logo: 'assets/kishron.png',
        estilos: {
            colorFondo: 'rgba(0,255,0,0.5)',
            colorTexto: 'yellow'
        }
    };
    profileImage = null;

    // menuLeft: any[] = this.mainMenu;

    constructor(private cd: ChangeDetectorRef) { }

    ngOnInit(): void {
        this.logged = this.getLogged();
        this.cd.detectChanges();
    }

    collapseHeader(event: boolean) {
        this.extendedHeader = !event;
    }

    openMenuLeft() {
        this.sidenav.open();
    }

    closeSession(event) {
        this.header.toggle();
    }

    onEditImage() {
        console.log('edicion de la imagen de perfil');
    }

    profileCardOption(item: any) {
        console.log(item);
        console.log(this.header);
        this.header.toggle();
    }

    mainMenuOption(item: any) {
        console.log(item);
    }

    emulateLogin() {
        const logged = this.getLogged();
        this.setLogged(logged ? '0' : '1');
        this.logged = !logged;
        this.cd.detectChanges();
    }

    onAction(item: any) {
        console.log(item);
    }

    onOption(item: any) {
        console.log(item);
        this.sidenav.close();
    }

    setLogged(item: string) {
        window.sessionStorage.setItem('extended', item);
    }

    getLogged(): boolean {
        return window.sessionStorage.getItem('extended') === null ?
            false : window.sessionStorage.getItem('extended') === '1' ? true : false;
    }

    setSize(item: MatSliderChange) {
        this.size = item.value;
        console.log(item);
    }

}
