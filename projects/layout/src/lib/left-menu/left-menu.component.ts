import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ly-left-menu',
    templateUrl: './left-menu.component.html',
    styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

    @Input() data: any[] = [];
    @Output() option: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    onOption(item: any) {
        this.option.emit(item);
    }

}
