import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Input,
    ViewChild
} from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { SatPopover } from '@ncstate/sat-popover';

@Component({
    selector: 'ly-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @Output() openMenuLeft: EventEmitter<any> = new EventEmitter();
    @Output() collapseHeader: EventEmitter<boolean> = new EventEmitter();
    @Output() action: EventEmitter<string> = new EventEmitter();
    @Input() messages = 0;
    @Input() cliente = {
        estilos: {
            colorFondo: 'red',
            colorTexto: 'yellow'
        },
        nombre: '',
        logo: ''
    };
    @Input() logged = false;
    @Input() image = null;

    @ViewChild(SatPopover, { static: true }) popover: SatPopover;

    position1 = 'below before';
    position2 = 'below after';
    mObserver: Subscription;

    constructor(private mediaObserver: MediaObserver) { }

    ngOnInit() {
        this.mObserver = this.mediaObserver.asObservable().pipe(
            map((elements: MediaChange[]) => {
                return elements.filter((element) => {
                    return (element.mqAlias === 'xs' || element.mqAlias === 'sm');
                });
            })
        ).subscribe(
            (media) => {
                if (media.length >= 1) {
                } else {
                }
            }
        );
    }

    onOpenMenuLeft() {
        this.openMenuLeft.emit();
    }

    toggle() {
        this.popover.toggle();
    }

    onAction(item) {
        this.action.emit(item);
    }

}
