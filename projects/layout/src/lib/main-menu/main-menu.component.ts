import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'ly-main-menu',
    templateUrl: './main-menu.component.html',
    styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

    @Output() option: EventEmitter<any> =  new EventEmitter();

    @Input() options: any[] = [];

    mObserver: Subscription;
    compact = true;

    constructor(private mediaObserver: MediaObserver) { }


    ngOnInit() {
        this.mObserver = this.mediaObserver.asObservable().pipe(
            map((elements: MediaChange[]) => {
                return elements.filter((element) => {
                    return (element.mqAlias === 'xs' || element.mqAlias === 'sm');
                });
            })
        ).subscribe(
            (media) => {
                this.compact = (media.length >= 1) ? true : false;
            }
        );
    }

    onClick(selectedItem: any) {
        this.option.emit(selectedItem);
        this.options.forEach((item) => {
            if (item === selectedItem) {
                item.active = true;
            } else {
                item.active = false;
            }
        });
    }

}
