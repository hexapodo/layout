import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SatPopoverModule } from '@ncstate/sat-popover';

import { LayoutComponent } from './layout.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { HeaderComponent } from './header/header.component';
import { ProfileCardComponent } from './profile-card/profile-card.component';

@NgModule({
  declarations: [
    LayoutComponent,
    LeftMenuComponent,
    MainMenuComponent,
    HeaderComponent,
    ProfileCardComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatRippleModule,
    MatBadgeModule,
    MatCardModule,
    MatListModule,
    MatTooltipModule,
    MatMenuModule,
    FlexLayoutModule,
    SatPopoverModule
  ],
  exports: [
    LayoutComponent,
    LeftMenuComponent,
    MainMenuComponent,
    HeaderComponent,
    ProfileCardComponent
  ]
})
export class LayoutModule { }
