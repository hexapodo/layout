import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ly-profile-card',
    templateUrl: './profile-card.component.html',
    styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {

    @Input() data: any = {};
    @Input() minimized = true;
    @Input() image = '';
    @Output() option: EventEmitter<any> = new EventEmitter();
    @Output() editImage: EventEmitter<any> = new EventEmitter();
    @Output() closeSession: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    onCloseSession() {
        this.closeSession.emit();
    }

    onOption(item) {
        this.option.emit(item);
    }

    onEditImage() {
        this.editImage.emit();
    }

}
